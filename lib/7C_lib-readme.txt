==========================================================================================
=========================CrevsDaak's WeiDU functions Library v1.5=========================
==========================================================================================

Contents:
  1. Introduction
  2. Special Thanks
  3. About using this library
  4. Guide on using this library
  5. Version history log


	1. The file '7C_lib.tph' contains a a library of different WeiDU patch, action
	   and macro functions that were created by CrevsDaak to facilitate his work in
	   the coding of some of his mods.


	2. Many, many thanks to the people keeping the IESDP up to date and to all of the
	   maintainers of WeiDU, for making such a good tool for IE modding, their work 
	   was what made all of this possible for me to do.


	3. You might *not* use this WeiDU function library _without_ Crevs' authorization,
	   but you are free to ask for permission to use it if you wish so ;)


	4. This library actually contains 5 different functions you can use:
		  I. rm_itm_align_use
		 II. edit_itm_abil_by_type
		III. edit_itm_eqeffect_by_opcode
		 IV. add_itm_ability
		  V. add_itm_to_item_use_2da
		 VI. 7c#center_bam_frames


	5. Version history:
	   v1.5, 13 December 2014:
		- More general bugfixing.
		- Renamed change_item_ability_value_per_field to edit_itm_abil_by_type.
		- Renamed del_align_use_itm_flags to rm_itm_align_use.
		- Added the (previously) standalone 7c#center_bam_frames(.tpa) to this
		  library.

	   v1.4, 12 December 2014:
		- General bugfixing.
		- Removed unnecessary code.

	   v1.3, 20 September 2014:
		- Added function add_itm_to_item_use_2da.

	   v1.2, 28 July, 2014:
		- Changed change_item_ability_byte to edit longs, shorts and ASCII
		  strings, and renamed it to change_item_ability_value_per_field
	          to reflect this changes.

	   v1.1, 23 July, 2014:
		- Added the add_item_ability patch function.
		- Fixed an error in the delete_align_flags patch function.
		- Renamed change_dice_size to change_item_ability_byte for better
		  identification & description of it's real function.
		- Renamed delete_align_flags to del_align_use_itm_flags for better
		  identification.

	   v1.0, 21 July, 2014:
		- Creation of the library with three functions:
		   + raise_lvl_per_class
		   + delete_align_flags
		   + change_dice_size
